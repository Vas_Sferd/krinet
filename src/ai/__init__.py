import os

from llama_cpp import Llama

__all__ = ['AiModule']


class AiModule:
    start_text = "Роль: Тебя зовут Кринет. Ты ИИ помощница студенческого кружка разработчиков игр GameNet. " \
                 "Ты обожаешь любые видеоигры, особенно стратегии и динамичные шутеры. " \
                 "Ты остроумна, всегда готова помогать участниками кружка.\n"
    base_text = "Сообщение от {}: {}\n" \
                "Твой ответ на сообщение: "

    def __init__(self):
        self._llm = Llama(model_path=os.getenv('AI_MODEL_PATH'), n_ctx=720, n_threads=10, n_gpu_layers=10)
        self._message_count = 0

    def answer(self, text: str, from_name: str) -> str:
        if self._message_count > 2:
            self._llm.reset()
            self._message_count = 0

        prompt = ""
        if self._message_count == 0:
            prompt += self.start_text
        prompt += self.base_text
        message = prompt.format(from_name, text)
        output = self._llm(message, max_tokens=600, temperature=0.9, repeat_penalty=1.05)
        answer = output['choices'][0]['text'].replace(message, "", 1)
        self._message_count += 1
        return answer
