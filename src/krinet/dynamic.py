import pkgutil

from importlib.machinery import ModuleSpec

from types import ModuleType
from typing import Dict, Type


def load_dynamic_modules(path: str, protocol: Type) -> Dict[str, ModuleType]:
    """
    :param path: Путь до директории с python файлами
    :param protocol: Протокол, который поддерживает операцию Runtime операцию isinstance()
    :return: Словарь модулей. В качестве ключей используется имя словаря
    """
    modules: Dict[str, ModuleType] = {}
    for module_finder, name, is_pkg in pkgutil.iter_modules(path=[path]):
        if is_pkg:
            continue

        spec: ModuleSpec | None = module_finder.find_spec(name, None)
        if spec is None:
            continue

        loader = spec.loader
        if loader is None:
            continue

        module: ModuleType = loader.load_module(name)
        if isinstance(module, (ModuleType, protocol)):
            modules[name] = module
    return modules
