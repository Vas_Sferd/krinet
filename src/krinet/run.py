#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# rum.py
#
# This is Krinet chat-bot run file.
#
# Copyright 2023 Vas Sferd <vassferd@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import time

from ai import AiModule
from vkontakte import Vk


def answering_vk():
    vk = Vk()
    ai = AiModule()
    names_cache = {}
    while True:
        for event in vk.next_longpoll_events():
            if isinstance(event, dict) and event['type'] == 'message_new':
                from_id: int = event['object']['message']['from_id']
                peer_id: int = event['object']['message']['peer_id']
                message: str = event['object']['message']['text']
                if from_id not in names_cache:
                    names_cache[from_id] = vk.users.get_users_name(from_id).expect('Данные пользователя недоступны')[0]
                if int(peer_id) < 2000000000 or 'кринет' in message.lower() or message.startswith('/'):
                    vk.messages.send(message=ai.answer(names_cache[from_id], message), peer_id=peer_id)
            time.sleep(1)


def run():
    answering_vk()


if __name__ == '__main__':
    run()
