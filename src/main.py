from api_tools import debug
from dotenv import load_dotenv
from krinet.run import run

load_dotenv()


def main():
	debug(f"Запуск программы в Режиме отладки")
	run()


if __name__ == '__main__':
	main()
