from functools import wraps
from typing import Callable, TypedDict, Unpack

import requests
from requests import HTTPError
from result import Result, Ok, Err

from api_tools import debug, parse_dict, DictParseError
from api_tools.api_provider import ApiProvider


class UnpackContentError(Exception):
    pass


ApiResponse = dict[str, object] | list[str | dict]
ApiError = HTTPError | UnpackContentError | DictParseError | ValueError
ApiResult = Result[ApiResponse, ApiError]

WrappedMethod = Callable[[...], ApiResult]


class ApiMethodPreferences(TypedDict, total=False):
    name: str
    url_format: str
    unpack_path: list[str]
    response_type: type[TypedDict]
    inject: Callable[[dict[str, object]], dict[str, object]]
    validate: Callable[[dict[str, object]], bool]
    default_params: dict[str, str]


def api_call(method: WrappedMethod | None = None, **preferences: Unpack[ApiMethodPreferences]):
    def decorator(mth: WrappedMethod) -> WrappedMethod:
        @wraps(mth)
        def wrapper(api_provider: ApiProvider, *args, **params) -> ApiResult:
            name: str = preferences.get('name', mth.__name__)
            url_format: str = preferences.get('url_format', api_provider.url_format())
            unpack_path: list[str] = preferences.get('unpack_path', api_provider.json_unpack_path())
            response_type: type[TypedDict] | None = preferences.get('response_type', None)
            inject: Callable[[dict[str, object]], dict[str, object]] | None = preferences.get('inject', None)
            validate: Callable[[dict[str, object]], bool] | None = preferences.get('validate', None)
            default_params: dict[str, str] = preferences.get('default_params', {})

            url = url_format.format(name)

            params.update(api_provider.default_params())
            params.update(default_params)
            params.update(dict(zip(mth.__code__.co_varnames[1:mth.__code__.co_argcount], args)))

            params = {k: v for k, v in params.items() if v is not None}

            if inject is not None:
                params = inject(params)

            if name.strip():
                debug(f"Параметры метода {name}", params)

            if validate and not validate(params):
                return Err(ValueError(f"Невалидные параметры запроса {params=}"))

            response = requests.get(url, params=params, headers=api_provider.headers())

            try:
                response.raise_for_status()
            except HTTPError as e:
                return Err(e)

            json_data = response.json()
            content = json_data
            for item in unpack_path:
                if item not in content:
                    return Err(UnpackContentError(
                        f"Нет пути {'/'.join(unpack_path)} (Участок: {item}) в json объекте: {json_data}"
                    ))
                content = content[item]

            debug('Получено событие:', content, method_name=name)
            if response_type is None:
                return Ok(content)
            match parse_dict(content, response_type):
                case Ok(parsed_content):
                    return Ok(parsed_content)
                case Err(e):
                    return Err(e)
            raise RuntimeError(f"Этот участок кода должен быть не достижим")
        return wrapper
    if method is None:
        return decorator
    return decorator(mth=method)
