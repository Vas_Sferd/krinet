import types
from typing import Any, TypedDict, NotRequired
from typing import cast, get_args, get_origin, get_type_hints

from result import Result, Ok, Err


class DictParseError(Exception):
    """Исключение связанное с проблемой извлечения корректных данных
    """

    def __init__(self, message, dont_parsed_data: dict[str: object], *args):
        super().__init__(message, *args)
        self._dont_parsed_data = dont_parsed_data

    def get_dont_parsed(self) -> dict[str, Any]:
        return self._dont_parsed_data

    def __str__(self):
        return f"{super().__str__()}, Некорректный объект: {self._dont_parsed_data}"


ParseDictResult = Result[dict, DictParseError]
ParseDictInnerResult = Result[Any, DictParseError | ValueError]


def parse_dict(source_dict: dict[str, object], expected_dict_type: type) -> ParseDictResult:
    def inner_parse(_d: object, _type: type) -> ParseDictInnerResult:
        if _d is None and isinstance(get_origin(_type), types.UnionType) and type(None) in get_args(_type):
            return Ok(_d)
        if get_origin(_type) == NotRequired:
            _type = get_args(_type)[0]
        if isinstance(_d, dict) and hasattr(_type, '__required_keys__') and hasattr(_type, '__optional_keys__'):
            # Безопасно, так как проверили подкласс
            return parse_dict(_d, cast(type[TypedDict], _type))
        else:
            try:
                if isinstance(_d, _type):
                    return Ok(_d)
                else:
                    return Err(ValueError(f"Значение {_d} явно не соответствует типу {_type}"))
            except TypeError:
                # Если не можем проверить типы, то и не надо
                return Ok(_d)

    source_keys = source_dict.keys()
    required_keys: frozenset[str] = expected_dict_type.__required_keys__
    optional_keys: frozenset[str] = expected_dict_type.__optional_keys__

    missed_keys = required_keys - source_keys

    if missed_keys:
        return Err(DictParseError(f"Не хватает ключей {list(missed_keys)}", dont_parsed_data=source_dict))

    hints: dict[str, type] = get_type_hints(expected_dict_type)
    res = source_dict

    extra_keys = source_keys - required_keys - optional_keys
    if extra_keys:
        res = {key: value for key, value in source_dict.items() if key not in extra_keys}

    for key, value in res.items():
        expected_local_type = hints[key]
        match inner_parse(value, expected_local_type):
            case Ok(_v):
                res[key] = _v
            case Err(inner_e):
                try:
                    raise DictParseError(f"Неверный тип {type(value)} для ключа '{key}'",
                                         dont_parsed_data=source_dict) from inner_e
                except DictParseError as e:
                    return Err(e)

    return Ok(res)
