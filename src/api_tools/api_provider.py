from typing import Protocol


class ApiProvider(Protocol):
	def url_format(self) -> str:
		"""Базовый Url форматируемый именем метода для создания полного URl api запроса
		"""

	def headers(self) -> dict[str, str]:
		"""HTTP заголовки по умолчанию
		"""

	def default_params(self) -> dict[str, str | float | int]:
		"""Параметры указываемые для всех запросов
		"""

	def json_unpack_path(self) -> list[str]:
		"""Путь для распаковки необходимых данных из json
		"""