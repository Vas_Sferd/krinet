import json
import os
from datetime import datetime


def _process_arg(arg: object, max_lines=50):
	if isinstance(arg, dict):
		arg = json.dumps(arg, ensure_ascii=False, indent=4)
	arg_str = str(arg)

	lines = arg_str.split('\n')
	if len(lines) <= max_lines:
		return arg_str

	prev_line = lines[max_lines // 2 - 1]
	leading_spaces = prev_line[:len(prev_line) - len(prev_line.lstrip())].count(' ')
	_ellipsis = ' ' * leading_spaces + '"..."'

	# Сохраняем начальную часть json
	starting_lines = lines[:max_lines // 2]

	# Сохраняем конечную часть json
	ending_lines = lines[-max_lines // 2:]

	# Собираем результирующую строку
	result = '\n'.join(starting_lines + [_ellipsis] + ending_lines)

	return result


def debug(*args, method_name=None, **kwargs):
	if bool(os.getenv("KRINET_DEBUG", default=False)):
		args = [_process_arg(arg) for arg in args]
		timestamp = datetime.now().strftime("%H:%M:%S")
		base_message = f"{timestamp}"
		if method_name is not None:
			base_message += f": {method_name}"
		print(f"[{base_message}] ", *args, **kwargs)
