import os

from result import Ok, Err

from vkontakte.groups import VkGroups
from vkontakte.longpoll import VkLongpoll
from vkontakte.messages import VkMessages
from vkontakte.users import VkUsers


class Vk:
    """Апи ВКонтакте
    """
    def __init__(self, group_mode=True):
        access_token = os.getenv('VK_API_TOKEN')
        api_version = os.getenv('VK_API_VERSION')
        group_id = os.getenv('VK_GROUP_ID')
        user_id = os.getenv('VK_USER_ID')

        if group_mode and group_id is not None:
            self_id = group_id
            access_token = os.getenv('VK_GROUP_API_TOKEN', access_token)
        elif not group_mode and group_id:
            self_id = user_id
            access_token = os.getenv('VK_USER_API_TOKEN', access_token)
        else:
            raise ValueError(
                f"Некорректные параметры. Выбран режим {'группы' if group_mode else 'пользователя'}, но id отсутствует"
            )

        self._group_mode = group_mode
        self._default_params = {'v': api_version, ('group_id' if group_mode else 'user_id'): self_id}
        self._headers = {'Authorization': f'Bearer {access_token}'}
        self._longpoll = None

        self.groups = VkGroups(self._headers, self._default_params)
        self.messages = VkMessages(self._headers, self._default_params)
        self.users = VkUsers(self._headers, self._default_params)

    def _new_longpoll_connect(self) -> VkLongpoll:
        if self._group_mode:
            longpoll_data = self.groups.get_longpoll_server().expect('Ошибка получения данных для Longpoll сессии')
        else:
            longpoll_data = self.messages.get_longpoll_server() \
                .expect('Ошибка получения данных для Longpoll сессии')
        return VkLongpoll(**longpoll_data)

    @property
    def longpoll(self):
        if self._longpoll is None:
            self._longpoll = self._new_longpoll_connect()
        return self._longpoll

    def next_longpoll_events(self, reconnect=True) -> list[dict[str, object]]:
        res = self.longpoll.listen_next()
        match res:
            case Ok(events):
                return events
            case Err(_):
                if not reconnect:
                    res.expect("Похоже, что сессия закончилась")
                self._new_longpoll_connect()
                return self.next_longpoll_events(reconnect=False)
