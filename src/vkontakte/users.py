from result import Err, Ok

from api_tools import api_call, ApiResult

from .dto import UserData
from .methods import VkMethods


class VkUsers(VkMethods):
    def __init__(self, headers: dict[str, str], default_parameters: dict[str, str | int | float]):
        default_parameters = {'v': default_parameters['v']}
        super().__init__('users', headers, default_parameters)

    @api_call
    def get(self, user_ids: int | str) -> ApiResult:
        ...

    def get_users_name(self, user_ids: int | str) -> ApiResult:
        match self.get(user_ids):
            case Err(e):
                return Err(e)
            case Ok(users_data):
                users_data: dict[int, UserData]
                return Ok(
                    [f"{user_data['first_name']} {user_data['last_name']}" for user_data in users_data]
                )
