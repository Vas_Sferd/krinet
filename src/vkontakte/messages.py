import random

from api_tools import api_call, ApiResult

from .dto import LongPollServerData
from .methods import VkMethods


def inject_for_send(params: dict):
    random_id = random.randrange(2 ** 11)
    params['random_id'] = random_id
    if params['message'] == "":
        params['message'] = "Честно говоря, я не знаю даже, что сказать"
    peer_id = params['peer_id']
    chat_id = peer_id - 2000000000
    if chat_id >= 0:
        params['chat_id'] = chat_id
    else:
        params['user_id'] = peer_id
    return params


class VkMessages(VkMethods):
    def __init__(self, headers: dict[str, str], default_parameters: dict[str, str | int | float]):
        super().__init__('messages', headers, default_parameters)

    @api_call(inject=inject_for_send)
    def send(self, message: str, peer_id: int) -> ApiResult:
        ...

    @api_call(name='getLongPollServer', response_type=LongPollServerData)
    def get_longpoll_server(self, lp_version=3, need_pts=1):
        ...

    @api_call(name='getHistory')
    def get_history(self, offset, count, peer_id):
        ...
