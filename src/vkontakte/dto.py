from typing import TypedDict


class LongPollServerData(TypedDict):
    key: str
    server: str
    ts: str | int


class LongPollUpdates(TypedDict):
    ts: str | int
    updates: list[dict | list]


class UserData(TypedDict):
    id: int
    first_name: str
    last_name: str
    can_access_closed: bool
    is_closed: bool
