from typing import Unpack, Callable

from result import Ok, Err

from api_tools import ApiProvider, api_call, ApiResult, debug
from vkontakte import LongPollServerData, LongPollUpdates

_listen_default_params = {'act': 'a_check', 'wait': 25}


class VkLongpoll(ApiProvider):
    def __init__(self, **kwargs: Unpack[LongPollServerData]):
        self._key: str = kwargs['key']
        self._server: str = kwargs['server']
        self._ts: str = kwargs['ts']
        self._listen_next: Callable[[...], ApiResult] | None = None

        if not self._server.startswith('https://'):
            self._server = f"https://{self._server}"

    def url_format(self) -> str:
        return self._server + "{}"

    def headers(self) -> dict[str, str]:
        return {}

    def default_params(self) -> dict[str, str | float | int]:
        return {}

    def json_unpack_path(self) -> list[str]:
        return []

    def listen_next(self) -> ApiResult:
        if self._listen_next is None:
            @api_call(name='', default_params=_listen_default_params, response_type=LongPollUpdates)
            def _listen_next(s: VkLongpoll, ts: str, key: str) -> ApiResult:
                ...
            self._listen_next = _listen_next
        res = self._listen_next(self, self._ts, self._key)
        match res:
            case Ok(data):
                self._ts = data['ts']
                return Ok(data['updates'])
            case Err(e):
                debug("Была допущена ошибка")
                return Err(e)
