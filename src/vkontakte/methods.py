from api_tools import ApiProvider, api_call, ApiResult


class VkMethods(ApiProvider):
    def __init__(self, methods_group: str, headers: dict[str, str], default_parameters: dict[str, str | int | float],
                 allow_dynamic_methods=False):
        self._methods_group = methods_group
        self._headers = headers
        self._default_parameters = default_parameters
        self._allow_dynamic_methods = allow_dynamic_methods

    def __getattr__(self, name):
        if not self._allow_dynamic_methods or name in self.__dict__:
            return self.__dict__[name]

        def dynamic(**kwargs) -> ApiResult:
            ...

        dynamic.__name__ = name
        method = api_call(dynamic)
        self.__dict__[name] = method
        return method

    def url_format(self) -> str:
        return f"https://api.vk.com/method/{self._methods_group}." + "{}"

    def headers(self) -> dict[str, str]:
        return self._headers

    def default_params(self) -> dict[str, str | float | int]:
        return self._default_parameters

    def json_unpack_path(self) -> list[str]:
        return ['response']
