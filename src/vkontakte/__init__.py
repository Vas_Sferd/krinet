from .dto import *
from .vk import Vk

__all__ = ['Vk', 'LongPollServerData', 'LongPollUpdates']
