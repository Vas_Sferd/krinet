from api_tools import api_call, ApiResult

from vkontakte.dto import LongPollServerData
from vkontakte.methods import VkMethods


class VkGroups(VkMethods):
    def __init__(self, headers: dict[str, str], default_parameters: dict[str, str | int | float]):
        super().__init__('groups', headers, default_parameters)

    @api_call(name='getLongPollServer', response_type=LongPollServerData)
    def get_longpoll_server(self) -> ApiResult:
        ...
