from typing import AsyncIterator, Protocol, runtime_checkable

from event import Event


@runtime_checkable
class Emitter(Protocol, AsyncIterator[Event]):
    """Источник событий, то что генерирует события
    """