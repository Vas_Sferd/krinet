from typing import Callable, Protocol, runtime_checkable

from event import Event

EventClassName = str
IsEvent = Callable[[Event, EventClassName], bool]
Answerer = Callable[[EventClassName, ...], Event | None]


@runtime_checkable
class Handler(Protocol):
    """Протокол обработчика, который вы должны реализовать неявно
    Лучше не использовать здесь долгие блокирующие вызовы
    """
    def handle(self, event: Event, answerer: Answerer) -> Event | None:
        """Обработка события.
        :param event: Событие, которое необходимо обработать.
        :param answerer: Необходимо использовать для создания ответного события из имени класса события и параметров
        его конструктора.
        :return: Возвращайте новый ивент или None, если нет ответного события.
        """