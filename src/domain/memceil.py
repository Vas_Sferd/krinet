from typing import Any, Literal, Protocol, runtime_checkable

from handler import Handler

StorageType = Literal["None", "File"]


@runtime_checkable
class MemCeil(Protocol, Handler):
    """Хранилище данных, которое может хранить некоторые данные от запуска к запуску.
    """
    def storage(self) -> StorageType:
        """Используется для определения формата предоставляемого хранилища данных"""
    def save(self, storage: Any) -> None:
        """Сохранение данных.
        :param storage: Хранилище, соответствующего типа
        """
    def load(self, storage: Any | None) -> None:
        """ Выгрузка данных из хранилища.
        :param storage: Хранилище. Доступность хранилища не гарантируется.
        """
