from typing import Protocol, runtime_checkable, Type


@runtime_checkable
class Event(Protocol):
    """Протокол ивента - по сути, любой тип, что может обрабатываться, как событие
    """
    def signature(self) -> str:
        """Возвращает зарегистрированный тип протокола
        """
