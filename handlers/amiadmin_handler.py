from legacy.abstract import *


class AmIAdminHandler(AbstractMessageHandler):
    @property
    def handler_name(self) -> str:
        return "AmIAdmin Handler"

    @property
    def priority(self) -> int:
        return 45

    def handle(self, message_event: AbstractMessageEvent, memory: Dict[str, AbstractMemoryCeil]) -> Optional[Callable]:
        try:
            text: str = message_event.message.text
            from_peer: Optional = message_event.message.from_peer
            from_user = message_event.message.from_user
            if "я" in text.lower() and "админ?" in text.lower():
                answer: str
                if from_user.is_admin:
                    answer = "Да, вы администратор"
                else:
                    answer = "Нет, вы не администратор"

                if from_peer is not None:
                    return lambda: message_event.api.send_to_peer(from_peer, answer)
                else:
                    return lambda: message_event.api.send_to_user(from_user, answer)
            else:
                return None
        except Exception as e:
            print(e)
            return None
