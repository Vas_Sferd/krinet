from legacy.abstract import *


class MailingHandler(AbstractMessageHandler):
    @property
    def handler_name(self) -> str:
        return "Mailing Handler"

    @property
    def priority(self) -> int:
        return 50

    def handle(self, message_event: AbstractMessageEvent, memory: Dict[str, AbstractMemoryCeil]) -> Optional[Callable]:
        try:
            text: str = message_event.message.text
            from_peer: Optional = message_event.message.from_peer
            if text.startswith("/all") and from_peer is not None and message_event.message.from_user.is_admin:
                return lambda: message_event.api.send_to_peer_members(from_peer, text[4::])
            else:
                return None
        except Exception as e:
            print(e)
            return None
