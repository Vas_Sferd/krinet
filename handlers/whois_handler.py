import random

from legacy.abstract import *


random.seed()


class WhoIsHandler(AbstractMessageHandler):
    @property
    def handler_name(self) -> str:
        return "WhoIs Handler"

    @property
    def priority(self) -> int:
        return 25

    def handle(self, message_event: AbstractMessageEvent, memory: Dict[str, AbstractMemoryCeil]) -> Optional[Callable]:
        try:
            text: str = message_event.message.text
            from_peer: Optional = message_event.message.from_peer
            if "кринет" in text.lower() and "кто" in text.lower() and "?" in text:
                if from_peer is not None:
                    who: str = random.choice(from_peer.members).name
                    if random.randint(0, 6) == 5:
                        who = "Кринет"

                    ans_start = random.choice(["Я считаю, что", "Нет сомнений, что", "Открою вам секрет, что"])

                    start: int = text.lower().find("кто") + 3
                    end: int = text.lower().find("?", start)

                    answer: str = ans_start + " " + who + " " + text[start:end:1]
                    return lambda: message_event.api.send_to_peer(from_peer, answer)
                else:
                    return None
            else:
                return None
        except Exception as e:
            print(e)
            return None
