import random

from legacy.abstract import *


random.seed()


class IsItTrueHandler(AbstractMessageHandler):
    @property
    def handler_name(self) -> str:
        return "IsItTrueHandler Handler"

    @property
    def priority(self) -> int:
        return 24

    def handle(self, message_event: AbstractMessageEvent, memory: Dict[str, AbstractMemoryCeil]) -> Optional[Callable]:
        try:
            text: str = message_event.message.text
            from_peer: Optional = message_event.message.from_peer
            if "кринет" in text.lower() and "правда ли" in text.lower() and "?" in text:
                answer: str = random.choice([
                    "Да, так все и есть",
                    "Это вообще-то был секрет, но да, это правда",
                    "Да, это правда. Ну и что?",
                    "Это правда, точно-преточно!",
                    "Это правда, точно-преточно! Инфасоточка!",
                    "Будь я Оракулом, то сказала бы тебе, что это правда. Впрочем, так и есть",
                    "Чушь собачья",
                    "Тебе наврали, " + message_event.message.from_user.name,
                    "Открой свои глаза, тебя обманули!",
                    "Да ты гонишь, это ерунда. Поверь, я знаю",
                    "Не будь невеждой, это ложь",
                    "Кто я такая, чтобы это знать",
                    "Ты сам знаешь ответ"
                ])

                if from_peer is not None:
                    return lambda: message_event.api.send_to_peer(from_peer, answer)
                else:
                    return lambda: message_event.api.send_to_user(message_event.message.from_user, answer)
            else:
                return None
        except Exception as e:
            print(e)
            return None
