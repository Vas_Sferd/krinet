from legacy.abstract import *


class HelloHandler(AbstractMessageHandler):
    @property
    def handler_name(self) -> str:
        return "Hello Handler"

    @property
    def priority(self) -> int:
        return 10

    def handle(self, message_event: AbstractMessageEvent, memory: Dict[str, AbstractMemoryCeil]) -> Optional[Callable]:
        try:
            text: str = message_event.message.text
            from_peer: Optional = message_event.message.from_peer
            from_user = message_event.message.from_user
            if "привет" in text.lower() and from_peer is None:
                return lambda: message_event.api.send_to_user(from_user, "Привет, я Кринет - официальный чат бот "
                                                                         "GameNET")
            elif "привет" in text.lower() and "кринет" in text.lower() and from_peer is not None:
                return lambda: message_event.api.send_to_peer(from_peer, "Категорически приветствую!")
            else:
                return None
        except Exception as e:
            print(e)
            return None
